var colors = require('colors');
const { includes } = require('./people');
const people = require('./people');
const { byGender, byMovie, byInterest, minimumS } = require('./trinker');
const tr = require('./trinker');
console.log(tr.title());
console.log("Model des données : ");
console.log(people[0]);
console.log(tr.line('LEVEL 1'));
console.log("Nombre d'hommes : ",                                                                   tr.nb(people,tr.byGender,"Male"));
console.log("Nombre de femmes : ",                                                                  tr.nb(people,tr.byGender,"Female"));
console.log("Nombre de personnes qui cherchent un homme :",                                         tr.nb(people,tr.byInterest,"M"));
console.log("Nombre de personnes qui cherchent une femme :",                                        tr.nb(people,tr.byInterest,"F"));
console.log("Nombre de personnes qui gagnent plus de 2000$ :",                                      tr.nb(people,tr.byIncome, 2000));
console.log("Nombre de personnes qui aiment les Drama :",                                           tr.nb(people,tr.byMovie,("Drama")));
console.log("Nombre de femmes qui aiment la science-fiction :",                                     tr.femaleSF(people));
console.log(tr.line('LEVEL 2'));
console.log("Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$ :",          tr.documentaryLovers(people));
console.log("Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$ :",      tr.listOfEarn(people));
console.log("Homme le plus riche (nom et id) :",                                                    tr.richestGuy(people));
console.log("Salaire moyen :",                                                                      tr.averageS(people));
console.log("Salaire médian :",                                                                     tr.medianS(people));
console.log("Nombre de personnes qui habitent dans l'hémisphère nord :",                             tr.nordPeople(people));
console.log("Salaire moyen des personnes qui habitent dans l'hémisphère sud :",                      tr.southPeople(people));
console.log(tr.line('LEVEL 3'));            
console.log("Personne qui habite le plus près de Bérénice Cawt (nom et id) :",                      tr.persPlusProches(people,people[60])[1]);
console.log("Personne qui habite le plus près de Ruì Brach (nom et id) :",                          tr.persPlusProches(people,people[557])[1]);
console.log("les 10 personnes qui habite les plus près de Josée Boshard (nom et id) :",              "create function".blue);
console.log("Les noms et ids des 23 personnes qui travaillent chez google :",                       tr.googlePeople(tr.byEmail(people,"google"))); 
console.log("Personne la plus agée :",                                                              tr.oldestPerson(people));
console.log("Personne la plus jeune :",                                                             tr.youngestPerson(people));
console.log("Moyenne des différences d'age :",                                                      "create function".blue);
console.log(tr.line('LEVEL 4'));            
console.log("Genre de film le plus populaire :",                                                    "create function".blue);
console.log("Genres de film par ordre de popularité :",                                             "create function".blue);
console.log("Liste des genres de film et nombre de personnes qui les préfèrent :",                  "create function".blue);
console.log("Age moyen des hommes qui aiment les films noirs :",                                    "create function".blue);
console.log(`Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire 
de Paris et gagnent moins que la moyenne des hommes :`,                                                 "create function".blue);
console.log(`Homme qui cherche un homme et habite le plus proche d'un homme qui a au moins une 
préférence de film en commun (afficher les deux et la distance entre les deux):`,                   "create function".blue);
console.log("Liste des couples femmes / hommes qui ont les même préférences de films :",            "create function".blue);
console.log(tr.line('MATCH'));
/* 
    On match les gens avec ce qu'ils cherchent (homme ou femme).
    On prend en priorité ceux qui sont les plus proches.
    Puis ceux qui ont le plus de goût en commun.
    Pour les couples hétéroséxuel on s'assure que la femme gagne au moins 10% de moins que l'homme mais plus de la moitié. 
    (C'est comme ça que fonctionne les sites de rencontre ! https://editionsgouttedor.com/catalogue/lamour-sous-algorithme/)
    Les gens qui travaillent chez google ne peuvent qu'être en couple entre eux.
    Quelqu'un qui n'aime pas les Drama ne peux pas être en couple avec quelqu'un qui les aime.
    Quelqu'un qui aime les films d'aventure doit forcement être en couple avec quelqu'un qui aime aussi les films d'aventure.
    Le différences d'age dans un couple doit être inférieure à 25% (de l'age du plus agée des deux)
    ߷    ߷    ߷    Créer le plus de couples possibles.   ߷    ߷    ߷    
    ߷    ߷    ߷    Mesurez le temps de calcul de votre fonction   ߷    ߷    ߷    
    ߷    ߷    ߷    Essayez de réduire le temps de calcul au maximum   ߷    ߷    ߷    
*/
console.log("liste de couples à matcher (nom et id pour chaque membre du couple) :",             tr.match(people));