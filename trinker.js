module.exports = tr = {
    title: function (){
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function(title = "="){
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    byGender: function(p,param){
    return p.filter(i => i.gender === param);
    },
    //p is for an array of (people) and param is for the gender value ("male" ,"Female")
    // byGender tab  contains all "gender" which is filtered out from all people array
    //i is for each peoples of the array
    // what inters into byGender is all genders and the output is "Male" ,"Female"

    byInterest: function(p,interest){
        return p.filter(i => i.looking_for === interest);
        //interest is for what each people are looking for ("F","M")
        //byInterest stores all people's looking_for which is filtered from people array

    },
    byMovie: function(p,movie){
        return p.filter(i => i.pref_movie.includes(movie));
        //movie is  for a pref_movie genre (movie genre like "Drama","Comedy","", etc)
        //byMovie stores all genre  pref_movie which is filtered from array people
    },
    byIncome: function(p,n){
        return p.filter(i => tr.moreIncome(i) > n);
        //n is for numbers((amount)  of income)(input)
        //byIncome stores all filtered incomes of each peoples without the $ sign
        //the output is amount of income which is > n

    },
    moreIncome: function(i) {
        return parseFloat(i.income.slice(1));
        //slice is used to remove $ sign from the numbers in income
    },

    femaleSF(p){
        return  tr.byMovie(tr.byGender(p, "Female"), "Sci-Fi").length;
        //femaleSF contains all Females whose pref_movie is "Sci-Fi"
        //it is filtered byMovie and byGender
        //it returns the amount of female with pref_movie "Sci-Fi"
    },
    documentaryLovers(p){
      return tr.byMovie(tr.byIncome(p, "1482"), "Documentary").length;
      //this function returns the amount of people whose pref_movie is "Documentary" filtering ByMovie
      //and those who have income of > 1482 which is filtered using byIncome function
    },
 listOfEarn(p){
    return tr.byIncome(p,4000).map(i=>{
        //this function stores a list of people who earn > 4000 filtered using byIncome function
        //the output is the list of people  who earn > 4000 with their id,name,income and last_name
        return {
            id: i.id,
            name: i.first_name,
            income: i.income,
            lastName: i.last_name
        }
    })
 },

 richestGuy: function(p, param){
    //filters array people byGender param "Male"  create new array which contains only Males salary
    //then sort it in ascending order to find the highest number
    //the output is the id,income and last name of a Male who has highest income
    let x = this.byGender(p, "Male");
    let salaire = [] ;
    for(let elem of x){
        tampon = parseFloat(elem.income.slice(1))
        salaire.push([tampon, elem.id, elem.last_name])
    }
    salaire.sort(function(a, b) {
        return a[0] - b[0];
      });
    return salaire[salaire.length-1]
},

averageS: function(p){
    //this function contains the average income of all people
    //it return the average income
    let avarage = 0
    for(let elem of p){
        avarage += parseFloat(elem.income.slice(1))
    }
    return avarage/p.length
},
   
medianS: function(p){
    //this contains a median income of people 
    //returns the median income
  salaries = []
  for(let elem of p){
    earn = parseFloat(elem.income.slice(1))
    salaries.push(earn);
  }
  salaries.sort((a,b)=>
 a - b)
 let median;

//if else block to check for even or odd
if(salaries.length%2 != 0){
    //odd case

    //find middle index
    let middleIndex = Math.floor(salaries.length/2)

    //find median
    median = salaries[middleIndex]
}else{
    //even case

    //find middle index
    let middleIndex = Math.floor(salaries.length/2)

    //find median
    median = (salaries[middleIndex] + salaries[middleIndex - 1])/2
    return median
}
 
},
  nordPeople: function(p){
    //returns the length or amount of people who lives in north
    let northPeople = p.filter(p => p.latitude > 0);
    return northPeople.length;
  },

  southPeople: function(p){
    let sudpeople = p.filter(p => p.latitude < 0)
    let moyenne = 0 //declare moyenne qui part de 0
    for (let elem of sudpeople){ // POUR ( un element de p)
        moyenne += parseFloat(elem.income.slice(1)) // on ajoute tout les Float de Income à moyenne 0
    }
    return moyenne/sudpeople.length // on divise le resultat de moyenne par la taille du tableau
},

distanceEntre:function(p1,p2){
    //returns the distance between p1 and P2

    let a = p1.latitude-p2.latitude
    let b = p1.longitude-p2.longitude
    let d = Math.sqrt(Math.pow(a, 2)+ Math.pow(b,2))
    return d
},

persPlusProches: function(people, cible){
    //returns the point which is closest to cible
    let t = []
    for(let p of people){
        let d = this.distanceEntre(p, cible)
        t.push({personne: p, distance: d})
    }
    t = t.sort((a, b)=> a.distance - b.distance)
   return t
},
                  
   oldestPerson: function(p){
    //returns the oldest person from array (people) 
    //by calculating their birth_of_date by using current YYMMDD method
    p.sort((a,b)=> new Date(a.date_of_birth) - new Date(b.date_of_birth));
    return p[0];

   },
   youngestPerson: function(p){
    //returns the youngest person from array (people)
    //by calculating their birth_of_date by using current YYMMDD method
    p.sort((a,b)=> new Date(b.date_of_birth) - new Date(a.date_of_birth));
    return p[0];

   },
 
   byEmail: function(p,em) { 
    //filter people array by email
    
    return p.filter(i=>i.email.includes(em));
},
googlePeople: function(p){
    //returns list of people who works at google by id,email,name and last name
            return p.map(i=>{ 

                    let id= i.id;
                    let email= i.email;
                    let name= i.first_name;
                    let lastName= i.last_name;

                    return[id, name, email, lastName];
            })
        },

    nb: function(p,filtre,param){
        let filtré = filtre(p,param);
        return filtré.length;
    }

}